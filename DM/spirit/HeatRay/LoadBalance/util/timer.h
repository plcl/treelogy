#ifndef TIMER_H
#define TIMER_H
#include<stddef.h>
#include <sys/time.h>
#include<stdint.h>
class timer{
	public:
		timer();
		void           Start();
		void           Stop();
		double		GetTotTime();
		bool IsRunning();
		uint64_t		GetLastExecTime();
		double Reset();
	private:
		struct timeval  startTime;
		struct timeval  endTime;
		double totTime;
		bool running;
		uint64_t lastExTime;
};

#endif
