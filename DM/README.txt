This folder contains the distributed memory implementations of Treelogy benchmarks.
Some benchmarks are implemented using the spirit framework (Hegde et.al. ICS 2017) and hence, are present inside the spirit folder. 
